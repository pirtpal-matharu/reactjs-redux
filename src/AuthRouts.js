import React from 'react';
import { BrowserRouter, Link, Routes, Route, Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux'


// Global Css
import './App.css';


// Pages
import Login from './authPages/Login';
import Register from './authPages/Register';

function AuthRouts() {
	return (
		<BrowserRouter>
			{/* Global Header */}
			<nav>
				<ul>
					<li><Link to="/login">Login</Link></li>
					<li><Link to="/register">Register</Link></li>
				</ul>
			</nav>

			{/* All Routes */}
			<Routes>
				<Route exact path='/login' element={<Login />} />
				<Route path='/register' element={<Register />} /> 
				<Route path="*" element={<Navigate replace to="/login" />} />
			</Routes>
		</BrowserRouter>
	);
}

export default AuthRouts;