import React from 'react';
import { useSelector } from 'react-redux'


export default function Home() {

	const { firstName } = useSelector(state => state.authReducer.userDetails);
	const post = useSelector(state => state.authReducer.post);
	console.log({ post });



	return (
		<div>
			<h2>Home</h2>
			<div>
				<div> <strong>Name from login/register page :</strong> {firstName} </div>

				{
					post ?
						<>
							<br />
							<div> <strong>Post title fomr initial load from Dashboard : </strong> : {post.title} </div>
						</>
					: null
				}
			</div>
		</div>
	);
}