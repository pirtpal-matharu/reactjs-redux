// Login
export const setLogin = (value) => ({ type: 'LOGIN', value });
export const setUserDetails = (value) => ({ type: 'SET_USER_DETAILS', value });
export const setPost = (value) => ({ type: 'SET_POST', value });
export const resetAppData = (value) => ({ type: 'RESET_APP_DATA', value });