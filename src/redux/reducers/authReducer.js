const initialState = {
  loggedIn: false,
  userDetails: {},
  post: null
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    // Login
    case 'LOGIN': {
      return { ...state, loggedIn: action.value }
    }

    case 'SET_USER_DETAILS': {
      return { ...state, userDetails: action.value }
    }

		case 'SET_POST': {
			return { ...state, post: action.value }
    }

    case 'RESET_APP_DATA': {
      return initialState
    }

    // Default
    default: {
      return state;
    }
  }
};

// Exports
export default authReducer;