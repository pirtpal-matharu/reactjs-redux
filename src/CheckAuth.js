import React from 'react';
import { useSelector } from 'react-redux'


// Global Css
import './App.css';


// Pages
import Dashboard from './Dashboard';
import AuthRouts from './AuthRouts'; 

function CheckAuth() { 
	const loggedIn = useSelector(state => state.authReducer.loggedIn);
	console.log({ loggedIn });

	if (loggedIn) {
		return <Dashboard />
	} else {
		return <AuthRouts />
	}
}

export default CheckAuth;