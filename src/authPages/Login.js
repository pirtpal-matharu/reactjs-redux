import React from 'react';
import { useDispatch } from 'react-redux'
import { useNavigate } from "react-router-dom";

import { setLogin, setUserDetails } from '../redux/actions/authActions';


export default function Login() {
	const dispatch = useDispatch();
	let navigate = useNavigate();

	async function _onLogin() {
		try {
			return await fetch('https://dummyjson.com/users/4')
				.then(response => response.json())
				.then(json => {
					dispatch(setUserDetails(json))
					dispatch(setLogin(true))
					navigate("/");
				})
		} catch (error) {
			console.log(error)
		}
	}

	return (
		<div>
			<h2>Login</h2>
			<button onClick={() => _onLogin()}>
				Click To Login
			</button>
		</div>
	);
}