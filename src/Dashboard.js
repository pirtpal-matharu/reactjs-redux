import React, { useEffect } from 'react';
import { BrowserRouter, Link, Routes, Route } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'

import { setPost, resetAppData } from './redux/actions/authActions';


// Global Css
import './App.css';


// Pages
import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/Contact';

function App() {
	const dispatch = useDispatch();

	useEffect(() => {
		_onLoad()
	}, []);


	async function _onLoad() {
		try {
			return await fetch('https://dummyjson.com/posts/1')
				.then(response => response.json())
				.then(json => {
					dispatch(setPost(json))
				})
		} catch (error) {
			console.log(error)
		}
	}

	return (
		<BrowserRouter>
			{/* Global Header */}
			<nav>
				<ul>
					<li><Link to="/">Home</Link></li>
					<li><Link to="/about">About</Link></li>
					<li><Link to="/contact">Contact</Link></li>
					<li>
						<button onClick={() => dispatch(resetAppData())}>
							Logout
						</button>
					</li>

				</ul>
			</nav>

			{/* All Routes */}
			<Routes>
				<Route exact path='/' element={<Home />} />
				<Route path='/about' element={<About />} />
				<Route path='/contact' element={<Contact />} />
			</Routes>
		</BrowserRouter>
	);
}

export default App;